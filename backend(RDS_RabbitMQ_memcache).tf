resource "aws_db_subnet_group" "dbsubnetgroup" {
  name       = "dbsubnetgroup"
  subnet_ids = [var.Privatesubnet1, var.Privatesubnet2, var.Privatesubnet3]

  tags = {
    Name = "My DB subnet group"
  }
}

resource "aws_elasticache_subnet_group" "memcachesubnetgrp" {
  name       = "memcachesubnetgrp"
  subnet_ids = [var.Privatesubnet1, var.Privatesubnet2, var.Privatesubnet3]
}


#------------------------------------------------------------------------
resource "aws_mq_broker" "rabbitmq" {
  broker_name        = "rabbitmq"
  engine_type        = "RabbitMQ"
  engine_version     = "5.15.9"
  host_instance_type = "mq.t2.micro"
  security_groups    = [aws_security_group.backend.id]
  subnet_ids         = [var.Privatesubnet1]
  user {
    username = var.rmquser
    password = var.rmqpassword
  }
}

#------------------------------------------------------------------------------

resource "aws_elasticache_cluster" "memcache" {
  cluster_id           = "memcache"
  engine               = "memcached"
  node_type            = "cache.t2.micro"
  num_cache_nodes      = 1
  parameter_group_name = "default.memcached1.4"
  port                 = 11211
  subnet_group_name    = aws_elasticache_subnet_group.memcachesubnetgrp.name
  security_group_ids   = [aws_security_group.backend.id]
}

#------------------------------------------------------------------------------

resource "aws_db_instance" "mysqlDB" {
  allocated_storage      = 10
  db_name                = var.dbname
  engine                 = "mysql"
  storage_type           = "gp2"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  username               = var.dbuser
  password               = var.dbpass
  parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = true
  multi_az               = false
  db_subnet_group_name   = aws_db_subnet_group.dbsubnetgroup.name
  vpc_security_group_ids = [aws_security_group.backend.id]
}