resource "aws_security_group" "Beanstalk_LB" {
  name        = "Beanstalk_LB_SG"
  description = "Beanstalk_LB_SG rule"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description      = "open to http"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

#-------------------------------------------------------------
resource "aws_security_group" "BastionHost" {
  name        = "BastionHost_SG"
  description = "BastionHost_SG rule"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description      = "open to http"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

#-------------------------------------------------------------
resource "aws_security_group" "Beanstalk_EC2" {
  name        = "Beanstalk_EC2_SG"
  description = "Beanstalk_EC2_SG rule"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description     = "open to http"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.BastionHost.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

#-------------------------------------------------------------
resource "aws_security_group" "backend" {
  name        = "backend_SG"
  description = "backend_SG rule"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description     = "open to http"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.Beanstalk_EC2.id, aws_security_group.BastionHost.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}


#-------------------------------------------------------------
resource "aws_security_group_rule" "example" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = aws_security_group.backend.id
  source_security_group_id = aws_security_group.backend.id
}

