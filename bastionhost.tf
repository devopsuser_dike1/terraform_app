resource "aws_instance" "bastionhost" {
  ami                    = lookup(var.AMI, var.AWSregion)
  instance_type          = var.insttype
  key_name               = aws_key_pair.terraformPublicKey.key_name
  availability_zone      = var.zone1
  vpc_security_group_ids = [aws_security_group.BastionHost.id]
  subnet_id              = module.vpc.public_subnets[0]
  count                  = var.instancecount
  tags = {
    Name = "bastionhost"
  }
  provisioner "file" {
    content     = templatefile("script.tmpl", { rds-endpoint = aws_db_instance.mysqlDB.address, dbuser = var.dbuser, dbpass = var.dbpass })
    destination = "/tmp/script.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sudo /tmp/script.sh"
    ]
  }

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file(var.private_key)
    host        = self.public_ip
  }

  depends_on = [aws_db_instance.mysqlDB]
}


