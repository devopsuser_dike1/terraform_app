variable "AWSregion" {
  default = "us-west-2"
}

variable "AMI" {
  type = map(any)
  default = {
    us-west-2 = "ami-0be50262c078dfea9"
    us-west-1 = "ami-0be50262c078dfea12"
  }
}

variable "private_key" {
  default = "terraform"
}

variable "public_key" {
  default = "terraform.pub"
}

variable "username" {
  default = "ec2-user"
}

variable "rmquser" {
  default = "test"
}


variable "rmqpassword" {
  default = "test_123456789"
}

variable "dbname" {
  default = "accounts"
}

variable "dbuser" {
  default = "admin"
}

variable "dbpass" {
  default = "admin123"
}

variable "instancecount" {
  default = "1"
}

variable "VPCName" {
  default = "APP"
}

variable "Privatesubnet1" {
  default = "172.21.1.0/24"
}

variable "Privatesubnet2" {
  default = "172.21.2.0/24"
}
variable "Privatesubnet3" {
  default = "172.21.3.0/24"
}

variable "Publicsubnet1" {
  default = "172.21.4.0/24"
}

variable "Publicsubnet2" {
  default = "172.21.5.0/24"
}

variable "Publicsubnet3" {
  default = "172.21.6.0/24"
}

variable "zone1" {
  default = "us-west-2a"
}

variable "zone2" {
  default = "us-west-2b"
}

variable "zone3" {
  default = "us-west-2c"
}

variable "vpccidr" {
  default = "172.21.0.0/16"
}

variable "insttype" {
  default = "t2.micro"
}