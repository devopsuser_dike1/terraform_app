resource "aws_key_pair" "terraformPublicKey" {
  key_name   = "terraformPublicKey"
  public_key = file("terraform.pub")
}
