module "vpc" {
  source                        = "terraform-aws-modules/vpc/aws"
  name                          = var.VPCName
  cidr                          = var.vpccidr
  azs                           = [var.zone1, var.zone2, var.zone3]
  private_subnets               = [var.Privatesubnet1, var.Privatesubnet2, var.Privatesubnet3]
  public_subnets                = [var.Publicsubnet1, var.Publicsubnet2, var.Publicsubnet3]
  create_database_subnet_group  = false
  manage_default_network_acl    = false
  manage_default_route_table    = false
  manage_default_security_group = false
  enable_dns_hostnames          = true
  enable_dns_support            = true
  enable_nat_gateway            = false
  single_nat_gateway            = false
  tags = {
    Environment = "staging"
    Name        = var.VPCName
  }
}